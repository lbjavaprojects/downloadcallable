
public class DownloadInfo {
	private long duration;
    private int size;
    private String path;
	public DownloadInfo(long duration, int size,String path) {
		super();
		this.duration = duration;
		this.size = size;
		this.path=path;
	}
	public String getFileName() {
		return path;
	}
	public long getDuration() {
		return duration;
	}
	public int getSize() {
		return size;
	}
    
}
