import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.concurrent.Callable;

public class DownloadWorker implements Callable<DownloadInfo> {
	 private String filePath;
	   public DownloadWorker(String filePath){
		  this.filePath=filePath;   
	   }
		@Override
		public DownloadInfo call() throws Exception {
			int size=0;
			long timeStart=System.currentTimeMillis();
			URL url=new URL(filePath);
			try(InputStream is=url.openStream();
					FileOutputStream fos=
					new FileOutputStream(dropExtention(extractFileName()));
					){
				int buffsize=1024,c;
				byte[] bufor=new byte[buffsize];
				System.out.println("�aduj� plik... "+extractFileName());
				while((c=is.read(bufor, 0, buffsize))>-1){
					fos.write(bufor, 0, c);
					size+=c;
				}
				}
			long timeStop=System.currentTimeMillis();
			return new DownloadInfo(timeStop-timeStart,size,extractFileName());
		}
		
	  private String extractFileName(){
		  return filePath.substring(filePath.lastIndexOf("/")+1);
	  }
	  
	  private String dropExtention(String fileName){
		  return fileName.substring(0,fileName.lastIndexOf("."));
	  }
}
